def hackathon():
    #Txt dosyasına göre ayarlanacak. CLASS
    class person():
        Id=0
        Ad="Ad"
        Soyad="Soyad"
        Numara=0
        Okul="Okul"
        def __init__(self,Id,Ad,Soyad,Numara,Okul):
            self.Id=Id
            self.Ad=Ad
            self.Soyad=Soyad
            self.Numara=Numara
            self.Okul=Okul
        def displayy(self):
            print("ID: "+self.Id,"AD: "+self.Ad,"SOYAD: "+self.Soyad,"NUMARA: "+self.Numara,"OKUL: "+self.Okul)

    def hata():
        print("Dosya bulunamadı!")
    #Txt dosyası toplam satır sayısını bulma
    try:
        okuma1 = open("okunacak.txt","r")
        satirsayisi=len(okuma1.readlines())
        okuma1.close()
    except FileNotFoundError:
        hata()

    #Txt dosyası satır değişken sayısını bulma
    try:
        okuma2 = open("okunacak.txt","r")
        satiricerigi=okuma2.readline()
        satiricerigi=okuma2.readline()
        dizi=satiricerigi.split("   ")
        degisken=len(dizi)
        okuma2.close()
    except FileNotFoundError:
        hata()

    w,h=degisken,satirsayisi
    liste1=[["" for y in range(w)] for x in range(h)]
    j=0

    p=person(0,"","",0,"")

    try:
        yaz=open("yazilacak.txt","w")
    except:
        print("Dosya Açılamadı!")

    try:
        okuma3 = open("okunacak.txt","r")

        for i in range(satirsayisi):
            satiricerigi=okuma3.readline()
            dizi=satiricerigi.split("   ")
            degisken=len(dizi)
            p.Id=dizi[j]
            p.Ad=dizi[j+1]
            p.Soyad=dizi[j+2]
            p.Numara=dizi[j+3]
            p.Okul=dizi[j+4]
            if(i==0):
                print("Değişkenler:")
            else:
                print("Sınıfa atanan öğrenci:")
            p.displayy()

            liste1[i][j]=p.Id
            liste1[i][j+1]=p.Ad
            liste1[i][j+2]=p.Soyad
            liste1[i][j+3]=p.Numara
            liste1[i][j+4]=p.Okul
            yaz.write(str(p.Id)+"   "+p.Ad+"   "+p.Soyad+"   "+str(p.Numara)+"   "+p.Okul)           
        okuma3.close()
        yaz.close()
    except FileNotFoundError:
        hata()
    
    print("Dizi Elemanları:")
    print(liste1)
    print("Öğrenci Sayısı:"+str(satirsayisi-1))
    x=int(input("Hangi sıradaki öğrenciyi görüntülemek istiyorsunuz:"))
    if(x>=1 and x<satirsayisi):
        print(x,". Sıradaki Kişi:")
        per=person(liste1[x][0],liste1[x][1],liste1[x][2],liste1[x][3],liste1[x][4])
        per.displayy()
    else:
        print("Kullanıcı hatası! (Girilebilecek sayı büyüklüğü: 1-"+str(satirsayisi-1)+" arası olmalıdır.)")

    try:
        no=int(input("Öğrenci numarası ile Ara: "))
        for i in range(satirsayisi):
            number=int(liste1[i+1][3])
            if(no==number):
                per=person(liste1[i+1][0],liste1[i+1][1],liste1[i+1][2],liste1[i+1][3],liste1[i+1][4])
                per.displayy()
                break
    except:
        print("Bu numaraya sahip bir öğrenci yok!")
hackathon()